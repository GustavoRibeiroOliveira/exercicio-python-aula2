class CalculaImposto(object):

    def __init__(self):
        self.__taxa = 0

    @property # metodo get - getter
    def taxa(self):
        return round(self.__taxa, 2)

    @taxa.setter # metodo set - setter
    def taxa(self, val_taxa):
        if val_taxa < 0:
            self.__taxa = 0
        elif val_taxa > 100:
            self.__taxa = 100
        else:
            self.__taxa = val_taxa
