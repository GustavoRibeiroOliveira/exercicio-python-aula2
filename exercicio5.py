class Veiculo(object):
    pass

    def __init__(self, marca, modelo, preco):
        self.marca = marca
        self.modelo = modelo
        self.preco = preco


class Carro(Veiculo):
    pass

    def __init__(self, marca, modelo, preco):
        super().__init__(marca, modelo, preco)


class Moto(Veiculo):
    pass

    def __init__(self, marca, modelo, preco):
        super().__init__(marca, modelo, preco)


class Caminhao(Veiculo):
    pass

    def __init__(self, marca, modelo, preco):
        super().__init__(marca, modelo, preco)


listaVeiculos = []

while True:
    print("\n\n-- Loja de Veículos --")
    print("1 - Listar Veiculos")
    print("2 - Comprar Veiculo")
    print("3 - Cadastrar Veiculo")
    print("4 - Sair")

    choice = int(input("Digite a opção: "))

    if choice == 1:
        idVeiculo = 1
        for veiculo in listaVeiculos:
            print("\nID do veiculo: ", idVeiculo)
            print("Marca do veiculo: ",veiculo.marca)
            print("Modelo do veiculo: ",veiculo.modelo)
            print("Preço do veiculo: ",veiculo.preco)
            idVeiculo += 1
    elif choice == 2:
        idVeiculo = 1
        for veiculo in listaVeiculos:
            print("\nID do veiculo: ", idVeiculo)
            print("Marca do veiculo: ", veiculo.marca)
            print("Modelo do veiculo: ", veiculo.modelo)
            print("Preço do veiculo: ", veiculo.preco)
            idVeiculo += 1
        nVeiculo = int(input("Digite o id do veiculo que deseja comprar na lista: "))
        listaVeiculos.pop(nVeiculo - 1)
    elif choice == 3:
        print("\n1 - Cadastrar carro")
        print("2 - Cadastrar moto")
        print("3 - Cadastrar caminhao")

        choice2 = int(input("Digite a opção: "))

        marca = input("Digite a marca do veiculo: ")
        modelo = (input("Digite o modelo do veiculo: "))
        preco = float(input("Digite o preço do veiculo: "))

        if choice2 == 1:
            listaVeiculos.append(Carro(marca, modelo, preco))
            print("Carro adicionado.")
        elif choice2 == 2:
            listaVeiculos.append(Moto(marca, modelo, preco))
            print("Moto adicionada.")
        elif choice2 == 3:
            listaVeiculos.append(Caminhao(marca, modelo, preco))
            print("Caminhão adicionado.")

    elif choice == 4:
        break
    else:
        print("Opção inválida!")
