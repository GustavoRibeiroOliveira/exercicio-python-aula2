class ContaBanco(object):

    def __init__(self, numeroConta, nomeCorentista, saldo=0.0):
        self.numeroConta = numeroConta
        self.nomeCorrentista = nomeCorentista
        self.saldo = saldo

    def alterar_nome(self, nome):
        self.nomeCorrentista = nome
        print("Nome do correntista alterado com sucesso.")

    def sacar(self, quantia):
        if quantia > 0:
            if self.saldo >= quantia:
                self.saldo -= quantia
                print("Saque efetuado com sucesso.")
            else:
                print("Saldo insuficiente.")
        else:
            print("Valor inválido.")

    def depositar(self, quantia):
        if quantia > 0:
            self.saldo += quantia
            print("Depósito efetuado com sucesso.")
        else:
            print("Valor inválido.")

    def mostrarDados(self):
        print("Número da conta: ",self.numeroConta)
        print("Nome do correntista: ",self.nomeCorrentista)
        print("Saldo da conta: ",self.saldo)


numeroConta = int(input("Digite o número da conta:"))
nomeCorrentista = input("Digite o nome do correntista:")

op = int(input("Gostaria de informar o saldo?\n Digite 1 para sim ou 2 para não: "))
if op == 1:
    saldo = float(input("Digite o saldo da conta:"))
    contaBanco_1 = ContaBanco(numeroConta, nomeCorrentista, saldo)
else:
    contaBanco_1 = ContaBanco(numeroConta, nomeCorrentista)

while True:
    print("\n-- Banco --")
    print("1 - Alterar nome do correntista")
    print("2 - Depositar")
    print("3 - Sacar")
    print("4 - Mostrar dados da conta")
    print("5 - Sair")

    choice = int(input("Digite a opção: "))

    if choice == 1:
        nome = input("Digite o novo nome do correntista:")
        contaBanco_1.alterar_nome(nome)
    elif choice == 2:
        quantia = float(input("Digite a quantia:"))
        contaBanco_1.depositar(quantia)
    elif choice == 3:
        quantia = float(input("Digite a quantia:"))
        contaBanco_1.sacar(quantia)
    elif choice == 4:
        contaBanco_1.mostrarDados()
    elif choice == 5:
        break
    else:
        print("Opção inválida!")