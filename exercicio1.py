class Homem(object):

    def __init__(self):
        print("Homem criado")


class Mulher(object):

    def __init__(self):
        print("Mulher criado")


pessoa = None
op = int(input("Digite 1 para homem e 2 para mulher: "))

if op == 1:
    pessoa = Homem()
elif op == 2:
    pessoa = Mulher()
else:
    print("Opção Inválida")
