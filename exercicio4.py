class ControladorDesconto(object):
    pass

    def __init__(self, preco_produto):
        self.preco_produto = preco_produto
        self.__porcentagem_desconto = 0

    @property  # metodo get - getter
    def porcentagem_desconto(self):
        return self.__porcentagem_desconto

    @porcentagem_desconto.setter  # metodo set - setter
    def porcentagem_desconto(self, desconto):
        if  desconto < 1:
            self.__porcentagem_desconto = 1
        elif desconto > 35:
            self.__porcentagem_desconto = 1
        else:
            self.__porcentagem_desconto = desconto


precoProduto = float(input("Digite o preço do produto: "))
descontoProduto = int(input("Digite a porcentagem de desconto do produto sem a %: "))

produto = ControladorDesconto(precoProduto)
produto.porcentagem_desconto = descontoProduto

print("O preço do produto original é: ",produto.preco_produto)
print(f"A porcentagem de desconto aplicada é {produto.porcentagem_desconto} %")
print("O preço do produto final é: ",produto.preco_produto*((100-produto.porcentagem_desconto)/100))