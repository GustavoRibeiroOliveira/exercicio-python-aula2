# class Aluno(object):
#
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#         self.nota_1 = 0.0
#         self.nota_2 = 0.0
#
#     def addNotas(self, nota_1, nota_2):
#         self.nota_1 = nota_1
#         self.nota_2 = nota_2
#
#     def media(self):
#         return (self.nota_1 + self.nota_2) / 2
#
#     def checaSituacao(self):
#         media = self.media()
#         if media >= 6:
#             print("Aprovado!")
#         elif media < 4:
#             print("Reprovado")
#         else:
#             print("NP3!")
#
#
# aluno_1 = Aluno("Leandro", 22)
# print(aluno_1.nome, aluno_1.idade)
#
# aluno_2 = Aluno("João", 25)
# aluno_2.addNotas(10, 7)
# print(aluno_2.nome, aluno_2.idade, aluno_2.media())
# aluno_2.checaSituacao()
#
# print(dir(Aluno))

# class Animal(object):
#     pass
#
# class Ave(Animal):
#     pass
#
# class BeijaFlor(Ave):
#     pass

# class Pai(object):
#     nome = "Carlos"
#     sobrenome = "Oliveira"
#     residencia = "Santa Rita"
#     olhos = "azuis"
#
# class Filha(Pai):
#     nome = "Luciana"
#     olhos = "castanhos"
#
# class Neta(Filha):
#     nome = "Maria"
#     olhos = "castanhos"
#
# print(Pai.nome, Filha.nome, Neta.nome)
# print(Pai.sobrenome, Filha.sobrenome, Neta.sobrenome)
# print(Pai.residencia, Filha.residencia, Neta.residencia)
# print(Pai.olhos, Filha.olhos, Neta.olhos)

# class Pessoa(object):
#     nome = None
#     idade = None
#
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#
#     def envelhecer(self):
#         self.idade += 1
#
#
# class Atleta(Pessoa):
#     peso = None
#     aposentado = None
#
#     def __init__(self, nome, idade, peso):
#         super().__init__(nome, idade)
#         self.peso = peso
#
#     def aquecer(self):
#         print("Atleta aquecido!")
#
#     def aposentar(self):
#         self.aposentado = True
#
#     def edit_nome(self, novoNome):
#         self.nome = novoNome
#
#
# class Corredor(Atleta):
#
#     def correr(self):
#         print("Correndo!!")
#
#
# class Nadador(Atleta):
#
#     def nadar(self):
#         print("Nadando!!")
#
#
# class Ciclista(Atleta):
#
#     def pedalar(self):
#         print("Pedalando!!")
#
#
# corredor_1 = Corredor("Leandro", 25, 80)
# nadador_1 = Nadador("João", 30, 75)
# ciclista_1 = Ciclista("Pedro", 21, 60)
#
# corredor_1.aquecer()
# corredor_1.correr()
# nadador_1.nadar()
# ciclista_1.pedalar()
#
#
# print(corredor_1.nome)
# corredor_1.edit_nome("Paulo")
# print(corredor_1.nome)


# def decorador(func):
#     def wrapper():
#         print("Antes de executar")
#         func()
#         print("Depois de executar")
#     return wrapper
#
#
# @decorador  # decorator
# def media_da_turma():
#     nota_1 = 10
#     nota_2 = 5
#     print(f"Média da turma = {(nota_1 + nota_2) / 2}")
#
#
# media_da_turma()

from classes import CalculaImposto

calc = CalculaImposto()

calc.taxa = 55.25
print(calc.taxa)

calc.taxa = 105.60
print(calc.taxa)